module.exports = function(app) {


    app.get("/", (req, res) => {  
        res.render("pages/login");
        });                              
        
        
    app.get("/home", (req, res) => {
            res.render("pages/home",  {
                title: "Home",
                pageDescription:"Home"
            });
              
        });

    app.get("/dashboard", (req, res) => {
        res.render("pages/dashboard",  {
        title: "About",
        pageDescription:"Dashboard"
         });
              
        });

     app.get("/register", (req, res) => {
            res.render("pages/register", { title: "Register",
          

        });
         });

         app.get("/menu", (req, res) => {
            res.render("pages/menu",  {
                title: "Menu",
             
            });
              
        });

        app.get("/main_data", (req, res) => {
            res.render("pages/main_data",  {
                title: "Main Data",
               
            });
              
        });
        app.get("/main_data_metrics", (req, res) => {
            res.render("pages/main_data_metrics",  {
                title: "Main Data Metrics",
               
            });
              
        });
        app.get("/head_circumference0to2", (req, res) => {
            res.render("pages/head_circumference0to2",  {
                title: "Head Circumference0to2",
               
            });
              
        });

        app.get("/head_circumference2Above", (req, res) => {
            res.render("pages/head_circumference2Above",  {
                title: "Head Circumference2Above",
               
            });
              
        });

        app.get("/weight-for-length", (req, res) => {
            res.render("pages/weight-for-length",  {
                title: "Weight-For-Length",
               
            });
              
        });

        app.get("/weight-for-height", (req, res) => {
            res.render("pages/weight-for-height",  {
                title: "Weight-For-Height",
               
            });
              
        });

        app.get("/weight-for-age0to2", (req, res) => {
            res.render("pages/weight-for-age0to2",  {
                title: "Weight-For-Age0to2",
               
            });
              
        });

        app.get("/weight-for-age2Above", (req, res) => {
            res.render("pages/weight-for-age2Above",  {
                title: "Weight-For-Age2Above",
               
            });
              
        });

        app.get("/length-for-age", (req, res) => {
            res.render("pages/length-for-age",  {
                title: "Length-For-Age0to2",
               
            });
              
        });

        app.get("/height-for-age", (req, res) => {
            res.render("pages/height-for-age",  {
                title: "Length-For-Age0to2",
               
            });
              
        });

        app.get("/growth_data_table", (req, res) => {
            res.render("pages/growth_data_table",  {
                title: "Growth Data Table",
               
            });
              
        });

        app.get("/profile", (req, res) => {
            res.render("pages/profile",  {
                title: "Profile",
               
            });
              
        });

        app.get("/table", (req, res) => {
            res.render("pages/table",  {
                title: "For test Only",
               
            });
              
        });
        
     
}
